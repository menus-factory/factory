-- Create Accounts table
CREATE TABLE accounts(
    id uuid NOT NULL,
    PRIMARY KEY (id),
    login TEXT NOT NULL,
    email TEXT NOT NULL UNIQUE,
    password TEXT NOT NULL,
    subscribed_at timestamptz NOT NULL
);
