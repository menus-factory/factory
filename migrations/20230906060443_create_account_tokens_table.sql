-- Create Account Tokens Table for subscriptions
CREATE TABLE account_tokens(
    account_token TEXT NOT NULL,
    account_id uuid NOT NULL
        REFERENCES accounts(id),
    PRIMARY KEY (account_token)
);
