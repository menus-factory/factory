BEGIN;
    -- Backfill 'status' for historical entries
    UPDATE accounts
        SET status = 'confirmed'
        WHERE status IS NULL;
    -- Make 'status' mandatory
    ALTER TABLE accounts ALTER COLUMN status SET NOT NULL;
COMMIT;
