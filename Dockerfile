FROM lukemathwalker/cargo-chef:latest-rust-1.71.1 as chef
WORKDIR /app

# Compute a lock-like file for the project
FROM chef as planner
WORKDIR /app
COPY . .
RUN cargo chef prepare --recipe-path recipe.json

# Build project dependencies (not the app !)
FROM chef as builder
COPY --from=planner /app/recipe.json recipe.json
RUN cargo chef cook --release --recipe-path recipe.json
COPY . .
ENV SQLX_OFFLINE true
RUN cargo build --release --bin factory

# Runtime stage
FROM debian:bullseye-slim AS runtime

WORKDIR /app
RUN apt-get update -y \
    && apt-get install -y --no-install-recommends openssl \
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*
COPY --from=builder /app/target/release/factory factory
COPY configuration configuration
ENV APP_ENVIRONMENT production
ENTRYPOINT ["./factory"]
