//! src/domain/account_email.rs

use validator::validate_email;

#[derive(Debug)]
pub struct AccountEmail(String);

impl AccountEmail {
    pub fn parse(s: String) -> Result<AccountEmail, String> {
        if validate_email(&s) {
            Ok(Self(s))
        } else {
            Err(format!("{} is not a valid account email.", s))
        }
    }
}

impl AsRef<str> for AccountEmail {
    fn as_ref(&self) -> &str {
        &self.0
    }
}

impl std::fmt::Display for AccountEmail {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

#[cfg(test)]
mod tests {
    use super::AccountEmail;
    use claim::assert_err;
    use fake::faker::internet::en::SafeEmail;
    use fake::Fake;

    #[test]
    fn empty_string_is_rejected() {
        let email = "".to_string();
        assert_err!(AccountEmail::parse(email));
    }

    #[test]
    fn email_missing_at_symbol_is_rejected() {
        let email = "testgeekcorp.fr".to_string();
        assert_err!(AccountEmail::parse(email));
    }

    #[test]
    fn email_missing_subject_is_rejected() {
        let email = "@geekcorp.fr".to_string();
        assert_err!(AccountEmail::parse(email));
    }

    #[derive(Debug, Clone)]
    struct ValidEmailFixture(pub String);

    impl quickcheck::Arbitrary for ValidEmailFixture {
        fn arbitrary<G: quickcheck::Gen>(g: &mut G) -> Self {
            let email = SafeEmail().fake_with_rng(g);
            Self(email)
        }
    }

    #[quickcheck_macros::quickcheck]
    fn valid_emails_are_parsed_successfully(valid_email: ValidEmailFixture) -> bool {
        AccountEmail::parse(valid_email.0).is_ok()
    }
}
