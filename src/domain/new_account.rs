//! src/domain/new_account.rs

use crate::domain::account_email::AccountEmail;
use crate::domain::account_login::AccountLogin;

pub struct NewAccount {
    pub email: AccountEmail,
    pub login: AccountLogin,
    pub password: String,
}
