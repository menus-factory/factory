//! src/routes/newsletters.rs

use crate::domain::AccountEmail;
use crate::email_client::EmailClient;
use crate::routes::error_chain_fmt;

use actix_web::http::StatusCode;
use actix_web::web;
use actix_web::{HttpResponse, ResponseError};
use anyhow::Context;
use sqlx::PgPool;

#[derive(thiserror::Error)]
pub enum PublishError {
    #[error(transparent)]
    UnexpectedError(#[from] anyhow::Error),
}

impl std::fmt::Debug for PublishError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        error_chain_fmt(self, f)
    }
}

impl ResponseError for PublishError {
    fn status_code(&self) -> StatusCode {
        match self {
            PublishError::UnexpectedError(_) => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}

#[derive(serde::Deserialize)]
pub struct BodyData {
    title: String,
    content: Content,
}

#[derive(serde::Deserialize)]
pub struct Content {
    html: String,
    text: String,
}

struct ConfirmedAccount {
    email: AccountEmail,
}

#[tracing::instrument(name = "Get confirmed accounts", skip(pool))]
async fn get_confirmed_accounts(
    pool: &PgPool,
) -> Result<Vec<Result<ConfirmedAccount, anyhow::Error>>, anyhow::Error> {
    let confirmed_accounts = sqlx::query!(
        r#"
        SELECT email
        FROM accounts
        WHERE status = 'confirmed'
        "#,
    )
    .fetch_all(pool)
    .await?
    .into_iter()
    .map(|r| match AccountEmail::parse(r.email) {
        Ok(email) => Ok(ConfirmedAccount { email }),
        Err(error) => Err(anyhow::anyhow!(error)),
    })
    .collect();
    Ok(confirmed_accounts)
}

pub async fn publish_newsletter(
    body: web::Json<BodyData>,
    pool: web::Data<PgPool>,
    email_client: web::Data<EmailClient>,
) -> Result<HttpResponse, PublishError> {
    let accounts = get_confirmed_accounts(&pool).await?;
    for account in accounts {
        match account {
            Ok(account) => {
                email_client
                    .send_email(
                        &account.email,
                        &body.title,
                        &body.content.html,
                        &body.content.text,
                    )
                    .await
                    .with_context(|| {
                        format!("Failed to send newsletter issue to {}", account.email)
                    })?;
            }
            Err(error) => {
                tracing::warn!(
                    error.cause_chain = ?error,
                    "Skipping a confirmed account. \
                    Their stored contact details are invalid.",
                );
            }
        }
    }
    Ok(HttpResponse::Ok().finish())
}
