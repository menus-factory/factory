//! src/routes/mod.rs

mod accounts;
mod accounts_confirm;
mod health;
mod newsletters;

pub use accounts::*;
pub use accounts_confirm::*;
pub use health::*;
pub use newsletters::*;
