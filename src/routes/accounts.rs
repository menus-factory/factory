//! src/routes/accounts.rs

use crate::domain::{AccountEmail, AccountLogin, NewAccount};
use crate::email_client::EmailClient;
use crate::startup::ApplicationBaseUrl;

use actix_web::http::StatusCode;
use actix_web::{web, HttpResponse, ResponseError};
use anyhow::Context;
use chrono::Utc;
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use sqlx::PgPool;
use sqlx::{Postgres, Transaction};
use uuid::Uuid;

#[derive(serde::Deserialize)]
pub struct FormData {
    login: String,
    email: String,
    password: String,
}

impl TryFrom<FormData> for NewAccount {
    type Error = String;

    fn try_from(value: FormData) -> Result<Self, Self::Error> {
        let email = AccountEmail::parse(value.email)?;
        let login = AccountLogin::parse(value.login)?;
        let password = value.password;
        Ok(Self {
            email,
            login,
            password,
        })
    }
}

#[derive(thiserror::Error)]
pub enum SubscribeError {
    #[error("{0}")]
    ValidationError(String),
    #[error(transparent)]
    UnexpectedError(#[from] anyhow::Error),
}

impl std::fmt::Debug for SubscribeError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        error_chain_fmt(self, f)
    }
}

impl ResponseError for SubscribeError {
    fn status_code(&self) -> StatusCode {
        match self {
            SubscribeError::ValidationError(_) => StatusCode::BAD_REQUEST,
            SubscribeError::UnexpectedError(_) => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}

#[tracing::instrument(
    name = "Adding a new account",
    skip(form, pool, email_client, base_url),
    fields(
        account_email = %form.email,
        account_login = %form.login
    )
)]
pub async fn subscribe(
    form: web::Form<FormData>,
    pool: web::Data<PgPool>,
    email_client: web::Data<EmailClient>,
    base_url: web::Data<ApplicationBaseUrl>,
) -> Result<HttpResponse, SubscribeError> {
    let new_account = form.0.try_into().map_err(SubscribeError::ValidationError)?;
    let mut transaction = pool
        .begin()
        .await
        .context("Failed to acquire a Postgres connection from the pool.")?;
    let account_id = insert_account(&mut transaction, &new_account)
        .await
        .context("Failed to insert new account in the database.")?;
    let account_token = generate_account_token();
    store_token(&mut transaction, account_id, &account_token)
        .await
        .context("Failed to store the confirmation token for a new account.")?;
    transaction
        .commit()
        .await
        .context("Failed to commit SQL transaction to store a new account.")?;
    send_confirmation_email(&email_client, new_account, &base_url.0, &account_token)
        .await
        .context("Failed to send confirmation email.")?;
    Ok(HttpResponse::Ok().finish())
}

#[tracing::instrument(
    name = "Store account token in the database",
    skip(account_token, transaction)
)]
pub async fn store_token(
    transaction: &mut Transaction<'_, Postgres>,
    account_id: Uuid,
    account_token: &str,
) -> Result<(), StoreTokenError> {
    sqlx::query!(
        r#"INSERT INTO account_tokens (account_token, account_id)
        VALUES ($1, $2)"#,
        account_token,
        account_id
    )
    .execute(transaction)
    .await
    .map_err(StoreTokenError)?;
    Ok(())
}

pub struct StoreTokenError(sqlx::Error);

impl std::fmt::Debug for StoreTokenError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        error_chain_fmt(self, f)
    }
}

impl std::fmt::Display for StoreTokenError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "A database error was encountered while \
            trying to store a subscription token."
        )
    }
}

impl std::error::Error for StoreTokenError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        // Compiler casts '&sqlx::Error' into '&dyn Error'
        Some(&self.0)
    }
}

pub fn error_chain_fmt(
    e: &impl std::error::Error,
    f: &mut std::fmt::Formatter<'_>,
) -> std::fmt::Result {
    writeln!(f, "{}\n", e)?;
    let mut current = e.source();
    while let Some(cause) = current {
        writeln!(f, "Caused by:\n\t{}", cause)?;
        current = cause.source();
    }
    Ok(())
}

#[tracing::instrument(
    name = "Send a confirmation email to a new subscriber",
    skip(email_client, new_account, base_url, account_token)
)]
pub async fn send_confirmation_email(
    email_client: &EmailClient,
    new_account: NewAccount,
    base_url: &str,
    account_token: &str,
) -> Result<(), reqwest::Error> {
    let confirmation_link = format!(
        "{}/accounts/confirm?account_token={}",
        base_url, account_token
    );
    let plain_body = format!(
        "Bienvenue sur votre fabrique a menus !\nVisitez {} pour confirmer votre inscription.",
        confirmation_link
    );
    let html_body = format!(
        "Bienvenue sur votre fabrique a menus !<br />\
         Cliquez <a href=\"{}\">ici</a> pour confirmer votre inscription.",
        confirmation_link
    );
    email_client
        .send_email(&new_account.email, "Bienvenue !", &html_body, &plain_body)
        .await
}

#[tracing::instrument(
    name = "Saving new account details in the database",
    skip(new_account, transaction)
)]
pub async fn insert_account(
    transaction: &mut Transaction<'_, Postgres>,
    new_account: &NewAccount,
) -> Result<Uuid, sqlx::Error> {
    let account_id = Uuid::new_v4();
    sqlx::query!(
        r#"
        INSERT INTO accounts (id, email, login, password, subscribed_at, status)
        VALUES ($1, $2, $3, $4, $5, 'pending_confirmation')
        "#,
        account_id,
        new_account.email.as_ref(),
        new_account.login.as_ref(),
        new_account.password,
        Utc::now()
    )
    .execute(transaction)
    .await?;
    Ok(account_id)
}

fn generate_account_token() -> String {
    let mut rng = thread_rng();
    std::iter::repeat_with(|| rng.sample(Alphanumeric))
        .map(char::from)
        .take(25)
        .collect()
}
