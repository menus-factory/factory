//! tests/api/accounts.rs

use crate::helpers::spawn_app;
use wiremock::matchers::{method, path};
use wiremock::{Mock, ResponseTemplate};

#[tokio::test]
async fn accounts_returns_200_for_valid_form_data() {
    // Arrange
    let app = spawn_app().await;
    let body = "login=Test%20User&email=test%40geekcorp.fr&password=Test";

    Mock::given(path("/email"))
        .and(method("POST"))
        .respond_with(ResponseTemplate::new(200))
        .expect(1)
        .mount(&app.email_server)
        .await;

    // Act
    let response = app.post_subscriptions(body.into()).await;

    // Assert
    assert_eq!(200, response.status().as_u16());
}

#[tokio::test]
async fn accounts_persists_the_new_account() {
    // Arrange
    let app = spawn_app().await;
    let body = "login=Test%20User&email=test%40geekcorp.fr&password=Test";

    // Act
    app.post_subscriptions(body.into()).await;

    // Assert
    let saved = sqlx::query!("SELECT email, login, password, status FROM accounts",)
        .fetch_one(&app.db_pool)
        .await
        .expect("Failed to fetch saved subscription.");

    assert_eq!(saved.email, "test@geekcorp.fr");
    assert_eq!(saved.login, "Test User");
    assert_eq!(saved.password, "Test");
    assert_eq!(saved.status, "pending_confirmation");
}

#[tokio::test]
async fn accounts_returns_400_when_fields_are_present_but_invalid() {
    // Arrange
    let app = spawn_app().await;
    let test_cases = vec![
        (
            "login=&email=test%40geekcorp.fr&password=Test",
            "empty name",
        ),
        ("login=Test%20User&email=&password=Test", "empty email"),
        //(
        //    "login=Test%20User&email=test%40geekcorp.fr&password=",
        //    "empty password",
        //),
        (
            "login=Test%20User&email=definitely-not-an-email&password=Test",
            "invalid email",
        ),
    ];

    for (body, description) in test_cases {
        // Act
        let response = app.post_subscriptions(body.into()).await;

        // Assert
        assert_eq!(
            400,
            response.status().as_u16(),
            "The API did not return a 400 OK when the payload was {}.",
            description
        );
    }
}

#[tokio::test]
async fn accounts_returns_400_when_data_is_missing() {
    // Arrange
    let app = spawn_app().await;
    let test_cases = vec![
        ("login=Test%20User&password=Test", "missing the email"),
        (
            "email=test%40geekcorp.fr&password=Test",
            "missing the login",
        ),
        (
            "login=Test%20User&email=test%40geekcorp.fr",
            "missing the password",
        ),
        ("", "missing name, email and password"),
    ];

    // Act
    for (invalid_body, error_message) in test_cases {
        let response = app.post_subscriptions(invalid_body.into()).await;

        // Assert
        assert_eq!(
            400,
            response.status().as_u16(),
            "The API did not fail with 400 Bad Request when the payload was {}.",
            error_message
        );
    }
}

#[tokio::test]
async fn accounts_sends_a_confirmation_email_for_valid_data() {
    // Arrange
    let app = spawn_app().await;
    let body = "login=Test%20&password=Test&email=test%40geekcorp.fr";

    Mock::given(path("/email"))
        .and(method("POST"))
        .respond_with(ResponseTemplate::new(200))
        .expect(1)
        .mount(&app.email_server)
        .await;

    // Act
    app.post_subscriptions(body.into()).await;

    // Assert
    // Mock asserts on drop
}

#[tokio::test]
async fn accounts_sends_a_confirmation_email_with_a_link() {
    // Arrange
    let app = spawn_app().await;
    let body = "login=Test%20&password=Test&email=test%40geekcorp.fr";

    Mock::given(path("/email"))
        .and(method("POST"))
        .respond_with(ResponseTemplate::new(200))
        .mount(&app.email_server)
        .await;

    // Act
    app.post_subscriptions(body.into()).await;

    // Assert
    let email_request = &app.email_server.received_requests().await.unwrap()[0];
    let confirmation_links = app.get_confirmation_links(&email_request);

    // The tow links should be identical
    assert_eq!(confirmation_links.html, confirmation_links.plain_text);
}

#[tokio::test]
async fn subscribe_fails_if_there_is_a_fatal_database_error() {
    // Arrange
    let app = spawn_app().await;
    let body = "login=Test%20&password=Test&email=test%40geekcorp.fr";
    // Sabotage
    sqlx::query!("ALTER TABLE account_tokens DROP COLUMN account_token;",)
        .execute(&app.db_pool)
        .await
        .unwrap();

    // Act
    let response = app.post_subscriptions(body.into()).await;

    // Assert
    assert_eq!(response.status().as_u16(), 500);
}
