# MenusFactory Factory

## Description
This is the backend of the Menus Factory project. It serves API REST and interacts with the database.

## Build

```
$ sudo docker build -t meuteubeu/menusfactory-factory . && \
  sudo docker push meuteubeu/menusfactory-factory
```

## Usage
TODO

## Roadmap
- Password encryption

## License
This project is licensed under the terms of the MIT license.

## Project status
On the starting line...
